<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lead_model extends CI_Model {

    public function __construct() {

        parent::__construct();
    }

    function allposts_count() {
        $query = $this
                ->db
                ->get('leads');

        return $query->num_rows();
    }

    function allposts($limit, $start, $col, $dir) {
        $query = $this
                ->db
                ->limit($limit, $start)
                ->order_by($col, 'DESC')
                ->get('leads');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    function posts_search($limit, $start, $search, $col, $dir) {
        $query = $this
                ->db
                ->like('id', $search)
                ->or_like('name', $search)
                ->limit($limit, $start)
                ->order_by($col, $dir)
                ->get('leads');


        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    function posts_search_count($search="") {
        $query = $this
                ->db
                ->like('id', $search)
                ->or_like('name', $search)
                ->get('leads');

        return $query->num_rows();
    }

    public function get_r($condi, $tbls, $fields, $limit, $tbl, $ordr = NULL) {
        $this->db->select($fields);
        if ($tbls != NULL) {
            foreach ($tbls as $tb) {
                $this->db->join($tb['name'], $tb['condi']);
            }
        }
        if ($condi != NULL) {
            foreach ($condi as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        if ($limit != '') {
            $this->db->limit($limit['limit'], $limit['start']);
        }
        if ($ordr != '') {
            $this->db->order_by('id', 'DESC');
        }
        $this->db->where('status', 1);
        $sql = $this->db->get($tbl);
        if ($sql->num_rows() > 0) {
            return $sql->result_array();
        } else {
            return false;
        }
    }
    public function getTodaysLead()
    {
        $this->db->select('id, name, report, region, website, mail');
        $this->db->from('leads');
        $this->db->like('created_at',date('Y-m-d'));
        $this->db->order_by('leads.id', 'DESC');
        $sql= $this->db->get();
        if ($sql->num_rows() > 0)
        {
            return $sql->result_array();
        }
        else
        {
            return false;
        }
    }
    public function getdatewiseleads($date1)
    {
        $this->db->select('id, name, report, region, website, mail');
        $this->db->from('leads');
        $this->db->like('created_at',$date1);
        $this->db->order_by('leads.id', 'DESC');
        $sql= $this->db->get();
        if ($sql->num_rows() > 0)
        {
            return $sql->result_array();
        }
        else
        {
            return false;
        }
    }

    public function getsitewiseleads($wsite)
    {
        $this->db->select('id, name, report, region, website, mail');
        $this->db->from('leads');
        $this->db->like('website',$wsite);
        $this->db->order_by('leads.id', 'DESC');
        $sql= $this->db->get();
        if ($sql->num_rows() > 0)
        {
            return $sql->result_array();
        }
        else
        {
            return false;
        }
    }


}
