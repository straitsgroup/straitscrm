<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_exc extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('data_model');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == 'Sales')
		{
			redirect(base_url().'Sales_exc/dashboard');
		}
		else
		{
			if ($_POST)
			{
				// print_r($_POST);
				if ($this->user_model->salesExeLogin($_POST))
				{
					redirect(base_url().'sales_exc/dashboard');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Invalid Credentials');
					redirect(base_url().'sales_exc');
				}
			}
			else
			{
				$data['pagetitle'] = 'Sales Login';
				$this->load->view('login', $data);
			}
		}
	}

	public function dashboard()
	{
		if ($this->session->userdata('logged_in') == 'Sales')
		{
			$new = $this->data_model->getNewLeadsUser();
			$closed = $this->data_model->getClosedLeadsUser();
			$current = $this->data_model->getSceduledLeadsUser();
			$data['NewLeads'] = $this->data_model->getNotificationUser();
			$data['pagetitle'] = 'Sales Executive Dashboard';
			$data['new'] = ($new) ? count($new) : 0;
			$data['closed'] = ($closed) ? count($closed) : 0;
			$data['current'] = ($current) ? count($current) : 0;

			$this->load->view('Sales_exc/dashboard', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function changePassword()
	{
		if ($this->session->userdata('logged_in') == 'Sales')
		{
			if ($_POST)
			{
				// print_r($_POST);
				if ($this->user_model->changePassword($_POST, 'sales') == "Success")
				{

					$this->session->set_flashdata('msg', 'Password Changed Successfully');
					redirect(base_url().'sales_exc/changePassword');
				}
				elseif($this->user_model->changePassword($_POST, 'sales') == "NotFound")
				{
					$this->session->set_flashdata('msg', 'Invalid Old Password');
					redirect(base_url().'sales_exc/changePassword');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Something went wrong Please try again');
					redirect(base_url().'sales_exc/changePassword');
				}
				
			}
			else
			{
				$data['NewLeads'] = $this->data_model->getNotificationUser();
				$data['pagetitle'] = "Change Password";
				$this->load->view('Sales_exc/changePassword', $data);
			}
		}
		else
		{
			
		}
		
	}
	
	public function details($id, $status)
	{
		$this->data_model->edit(array('seen' =>1), 'leads', $id);
		if ($status ==2)
		{
			$Record = $this->data_model->getById('leads', $id);
		}
		else
		{
			$Record = $this->data_model->getById('leads', $id);
		}
		echo '<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Lead Detaile</h4>
				</div>
				<div class="modal-body">
					<p>
						<div class="row">';
						foreach ($Record as $key => $value)
						{
							if ($key!= "id" && $key!="seen" && $key != "created_at")
							{
								echo '<div class="col-md-12 col-sm-12">
									<label class="col-md-3 col-sm-4">'.ucfirst($key).'</label>
									<label class="col-md-9 col-sm-8">'.$value.'</label>
								</div>';	
							}
							if ($key=="created_at")
							{
								echo '<div class="col-md-12 col-sm-12">
									<label class="col-md-3 col-sm-4">Lead Date</label>
									<label class="col-md-9 col-sm-8">'.date('d M Y h:i a', strtotime($value)).'</label>
								</div>';
							}
						}
						echo'</div>
					</p>
				</div>
		  		<div class="modal-footer">';
				if($status != 2)
				{
					echo'<a href="'.base_url().'sales_exc/edit/'.$id.'" type="button" class="btn btn-primary">Edit Lead</a>';
				}
				echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>';
	}
	
	public function list()
	{
		if ($this->session->userdata('logged_in') == 'Sales')
		{
			$data['Records'] = $this->data_model->getNewLeadsUser();
			$data['NewLeads'] = $this->data_model->getNotificationUser();
			$data['pagetitle'] = "New Leads";
			$this->load->view('Sales_exc/list', $data);
			// print_r($data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function closedList()
	{
		if ($this->session->userdata('logged_in') == 'Sales')
		{
			$data['Records'] = $this->data_model->getClosedLeadsUser();
			$data['NewLeads'] = $this->data_model->getNotificationUser();
			$data['pagetitle'] = "Closed Leads";
			$this->load->view('Sales_exc/list', $data);
			// print_r($data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function currentList()
	{
		if ($this->session->userdata('logged_in') == 'Sales')
		{
			$data['Records'] = $this->data_model->getSceduledLeadsUser();
			$data['NewLeads'] = $this->data_model->getNotificationUser();
			$data['pagetitle'] = "Current Leads";
			$this->load->view('Sales_exc/list', $data);
			// print_r($data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function followUp()
	{
		if ($this->session->userdata('logged_in') == 'Sales')
		{
			$data['Records'] = $this->data_model->getLeads_followup();
			$data['NewLeads'] = $this->data_model->getNotificationUser();
			$data['pagetitle'] = "follow Up Leads";
			$this->load->view('Sales_exc/followup', $data);
			//print_r($data['Records']);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function followupDetails($id)
	{
		if ($this->session->userdata('logged_in') == 'Sales')
		{
			$data['Records'] = $this->data_model->getlead_details($id);
			$data['NewLeads'] = $this->data_model->getNotificationUser();
			$data['pagetitle'] = "follow Up Leads Details";
			$this->load->view('Sales_exc/followup', $data);
			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function edit($id)
	{
		if ($this->session->userdata('logged_in') == 'Sales')
		{
			if ($_POST)
			{

				$config['upload_path'] = './uploads/recordings';
				$config['allowed_types'] = 'mp3|wav|aif|aiff|ogg';
				$config['max_size'] = 32768;
				

				$this->load->library('upload', $config);

				if ($this->upload->do_upload('recording_file'))
				{
					// $data = array('upload_data' => $this->upload->data());
					$_POST['recording_file'] = $this->upload->data()['file_name'];
					//print_r($this->upload->data()['file_name']);
				}
				else
				{
					$_POST['recording_file'] = NULL;
				}
				$_POST['followup_date'] = date("Y-m-d", strtotime($_POST['followup_date']));
				if ($_POST['followup'])
				{
					$_POST['status'] = 1;
					unset($_POST['followup']);
				}
				else
				{
					$_POST['followup_no'] = NULL;
					$_POST['followup_date'] = NULL;
					unset($_POST['followup']);
					$_POST['status'] = 2;
				}
				
				$_POST['lead_id'] = $id;
				$saleslead = $this->data_model->getsaleslead_id($id);
				if ($this->data_model->add($_POST, 'lead_details'))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
					$this->data_model->edit(array('status' => $_POST['status']), 'lead_sales', $saleslead['id']);
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'sales_exc/list');

			}
			else
			{
				
				$data['Record'] = $this->data_model->getById('leads', $id);
				$data['NewLeads'] = $this->data_model->getNotificationUser();
				$data['pagetitle'] = "Edit Lead";
				$data['categories'] =$this->data_model->getByCondition(array('field'=>'status','value'=>1),'category');
				$data['Publishers'] =$this->data_model->getByCondition(array('field'=>'status','value'=>1),'publisher');
				$data['sales_status'] =$this->data_model->getByCondition(array('field'=>'status','value'=>1),'sales_status');
				 
				$this->load->view('Sales_exc/edit', $data);
			}
			
			}
		else
		{
			redirect(base_url());
		}
	}
}