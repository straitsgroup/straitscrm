<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Lead_model');
		$this->load->library('excel');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')=='Admin')
		{
			if ($_POST)
			{
				$configUpload['upload_path'] = FCPATH.'uploads/excel/';
				$configUpload['allowed_types'] = 'xls|xlsx|csv';
				$configUpload['max_size'] = '5000';
				
				$this->load->library('upload', $configUpload);
				$this->upload->do_upload('excelFile');
				$upload_data = $this->upload->data();
				$file_name = $upload_data['file_name']; //uploded file name
				$extension=$upload_data['file_ext'];    // uploded file extension

				$objReader= PHPExcel_IOFactory::createReader('Excel2007');	// For excel 2007

				 //Set to read only
				$objReader->setReadDataOnly(true);
				
				//Load excel file
				$objPHPExcel=$objReader->load(FCPATH.'uploads/excel/'.$file_name);
				$totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
				$totalcol=$objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();   //Count Numbe of rows avalable in excel
				$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

				//loop from first data untill last data
				$count = 0;
				for($i=2;$i<=$totalrows;$i++)
				{
					$Array['name'] = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
					$Array['mail'] = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
					$Array['job_title'] = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
					$Array['company'] = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
					$Array['phone'] = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
					$Array['country'] = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
					$Array['message'] = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
					$Array['report'] = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
					$Array['region'] = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
					$Array['ip'] = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
					$Array['website'] = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
					$Array['source'] = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();

					if ($this->Lead_model->add($Array, "leads"))
					{
						$count++;
					}
				}
				unlink('././uploads/excel/'.$file_name); //File Deleted After uploading in database .
				if ($count == ($totalrows-1))
				{
					$this->session->set_flashdata('msg', $count.' Leads Uploaded Successfully');
					redirect(base_url().'leads');
				}
				else
				{
					$this->session->set_flashdata('msg', $count.' Leads Uploaded Successfully');
					redirect(base_url().'leads');
				}
			}
			else
			{
				$data['pagetitle'] = "Upload Leads";
				$this->load->view('leads/excel_import', $data);
			}
		}
		else
		{
			redirect(base_url());
		}	
	}

	public function add($Array, $table)
	{
		if ($this->db->insert($table, $Array))
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
	}


	/*Lead Listing*/

	public function viewlist() {
		if ($this->session->userdata('logged_in')=='Admin')
		{
			$data['pagetitle'] = 'All Leads';
			$this->load->view('leads/leadlist', $data);
		}
		else
		{
			redirect(base_url());
		}	
    }

    public function ajaxList() {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'mail',
            3 => 'report',
            4 => 'region',
            5 => 'created_at',
            6 => 'website'
        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];

        $totalData = $this->Lead_model->allposts_count();

        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $posts = $this->Lead_model->allposts($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];

            $posts = $this->Lead_model->posts_search($limit, $start, $search, $order, $dir);

            $totalFiltered = $this->Lead_model->posts_search_count($search);
        }

        $data = array();
        if (!empty($posts)) 
        {
            foreach ($posts as $post) 
            {
                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                $nestedData['mail'] = $post->mail;
                $nestedData['report'] = substr($post->report, 0,50) ;
                $nestedData['region'] = $post->region;
                $nestedData['created_at'] = $post->created_at;
                $nestedData['website'] = $post->website;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }
}