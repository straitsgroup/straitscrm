<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Administration | CRM</title>    
        <link rel="stylesheet" href="<?php echo base_url().'assets/' ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'assets/' ?>bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'assets/' ?>bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'assets/' ?>dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'assets/' ?>dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'assets/' ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'assets/' ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <script src="<?php echo base_url().'assets/' ?>bower_components/jquery/dist/jquery.min.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/> -->
        <link rel="icon" type="image/png" href="<?php echo base_url() . 'web_assets/' ?>img/favicon.png">
    </head>         
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="<?php echo base_url().'dashboard' ?>" class="logo">
                <span class="logo-mini"><b>CRM</b></span>
                <span class="logo-lg"><b>Straits</b> CRM</span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo base_url().'assets/' ?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo $this->session->userdata('name') ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php echo base_url().'assets/' ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                    <p>
                                        <?php echo $this->session->userdata('name') ?>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?php echo base_url().'logout/Admin' ?>" class="btn btn-default btn-flat">Log out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-chevron-circle-right"></i>
                            <span>Sales Executive</span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>sales"><i class="fa fa-circle-o"></i> List Sales Executive</a></li>
                            <li><a href="<?php echo base_url();?>sales/add"><i class="fa fa-circle-o"></i> Add Sales Executive</a></li>
                        </ul>
                    </li>
<!--                     <li class="treeview">
                        <a href="#">
                            <i class="fa fa-chevron-circle-right"></i>
                            <span>Marketing Team Leads</span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>marketing"><i class="fa fa-circle-o"></i> List Marketing Team Leads</a></li>
                            <li><a href="<?php echo base_url();?>marketing/add"><i class="fa fa-circle-o"></i> Add Marketing Team Leads</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-chevron-circle-right"></i>
                            <span>Marketing Executive</span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>marketing_agent"><i class="fa fa-circle-o"></i> List Marketing Executive</a></li>
                            <li><a href="<?php echo base_url();?>marketing_agent/add"><i class="fa fa-circle-o"></i> Add Marketing Executive</a></li>
                        </ul>
                    </li>
                     -->
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-chevron-circle-right"></i>
                            <span>Regions</span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>regions"><i class="fa fa-circle-o"></i> List Regions</a></li>
                            <li><a href="<?php echo base_url();?>regions/add"><i class="fa fa-circle-o"></i> Add Regions</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-chevron-circle-right"></i>
                            <span>Sales Status</span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>sales_status"><i class="fa fa-circle-o"></i> List Sales Status</a></li>
                            <li><a href="<?php echo base_url();?>sales_status/add"><i class="fa fa-circle-o"></i> Add Sales Status</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-chevron-circle-right"></i>
                            <span>Category</span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>category"><i class="fa fa-circle-o"></i> List Category</a></li>
                            <li><a href="<?php echo base_url();?>category/add"><i class="fa fa-circle-o"></i> Add Category</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-chevron-circle-right"></i>
                            <span>Publisher</span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>publisher"><i class="fa fa-circle-o"></i> List Publisher</a></li>
                            <li><a href="<?php echo base_url();?>publisher/add"><i class="fa fa-circle-o"></i> Add Publisher</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url().'leads' ?>">
                            <i class="fa fa-chevron-circle-right"></i><span>All Leads</span>
                        </a>
                    </li>
                </ul>
            </section>
        </aside>
        <div class="content-wrapper">
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo "Lead List" ?></h3>
                                <div class="pull-right">
                                </div>
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered" id="posts">
                                    <thead>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Report</th>
                                        <th>Region</th>
                                        <th>Date</th>
                                        <th>Website</th>
                                        <th>Sales Excutive</th>
                                        <th>Actions</th>
                                    </thead>			
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
            <!-- jQuery 3.1.1 -->
        <script src="<?php echo base_url() . 'assets/' ?>plugins/jQuery/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#posts').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "<?php echo base_url('Leads/viewlist') ?>",
                        "dataType": "json",
                        "type": "POST",
                        "data": {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}
                    },
                    "columns": [
                        {"data": "id"},
                        {"data": "name"},
                        {"data": "mail"},
                        {"data": "report"},
                        {"data": "region"},
                        {"data": "created_at"},
                        {"data": "website"}
                    ]

                });
            });
        </script>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.1
            </div>
            <strong>Copyright &copy; 2018-2020 <a href="#">Market Research Vision</a>.</strong> All rights
            reserved.
        </footer>
        <div class="control-sidebar-bg"></div>
    </div>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script>$.widget.bridge('uibutton', $.ui.button);</script>
    <script src="<?php echo base_url() . 'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo base_url() . 'assets/' ?>plugins/morris/morris.min.js"></script>
    <script src="<?php echo base_url() . 'assets/' ?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url() . 'assets/' ?>dist/js/adminlte.min.js"></script>
    <script src="<?php echo base_url() . 'assets/' ?>dist/js/pages/dashboard.js"></script>
    <script src="<?php echo base_url() . 'assets/' ?>dist/js/demo.js"></script>
</body>
</html>